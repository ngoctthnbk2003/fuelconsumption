import streamlit as st
import pandas as pd
import pickle

# Tải mô hình Random Forest Regression và tên các đặc trưng từ đĩa
with open('random_forest_model.pkl', 'rb') as f:
    rf_model, feature_names = pickle.load(f)

# Tiêu đề của app
st.title("Fuel Consumption Prediction")

# Thanh bên cho nhập liệu của người dùng
st.sidebar.header('User Input Parameters')

def user_input_features():
    engine_size = st.sidebar.number_input("Engine Size", value=2.0)
    cylinders = st.sidebar.number_input("Cylinders", value=4)
    emissions = st.sidebar.number_input("Emissions", value=200)
    hwy = st.sidebar.number_input("HWY (L/100 km)", value=8.0)
    comb = st.sidebar.number_input("COMB (L/100 km)", value=10.0)
    data = {
        'ENGINE SIZE': engine_size,
        'CYLINDERS': cylinders,
        'EMISSIONS': emissions,
        'HWY (L/100 km)': hwy,
        'COMB (L/100 km)': comb
    }
    features = pd.DataFrame(data, index=[0])
    return features

input_df = user_input_features()

# Đảm bảo các đặc trưng đầu vào có thứ tự giống như khi huấn luyện
input_df = input_df[feature_names]

# Hiển thị các tham số đầu vào
st.subheader('User Input Parameters')
st.write(input_df)

# Dự đoán sử dụng mô hình Rừng Ngẫu nhiên
prediction = rf_model.predict(input_df)

st.subheader('Prediction')
st.write(prediction)

# Tải và hiển thị 10 dòng ngẫu nhiên cùng với dự đoán
with open('random_rows.pkl', 'rb') as file:
    random_rows = pickle.load(file)

# Lặp qua mỗi dòng để trích xuất các đặc điểm và thực hiện dự đoán
predictions = []
for index, row in random_rows.iterrows():
    feature_data = {
        'ENGINE SIZE': row['ENGINE SIZE'],
        'CYLINDERS': row['CYLINDERS'],
        'EMISSIONS': row['EMISSIONS'],
        'HWY (L/100 km)': row['HWY (L/100 km)'],
        'COMB (L/100 km)': row['COMB (L/100 km)']
    }
    features_df = pd.DataFrame(feature_data, index=[0])
    features_df = features_df[feature_names]  # Sắp xếp đúng thứ tự các cột
    pred = rf_model.predict(features_df)[0]
    feature_data['Prediction'] = pred
    predictions.append(feature_data)

# Tạo DataFrame để hiển thị
display_df = pd.DataFrame(predictions)

# Thêm cột 'FUEL CONSUMPTION' vào sau cột 'Prediction'
fuel_consumption = random_rows['FUEL CONSUMPTION'].reset_index(drop=True)
display_df['FUEL CONSUMPTION'] = fuel_consumption

# Di chuyển cột 'FUEL CONSUMPTION' đến vị trí sau cột 'Prediction'
cols = list(display_df.columns)
cols.insert(cols.index('Prediction') + 1, cols.pop(cols.index('FUEL CONSUMPTION')))
display_df = display_df[cols]

# Hiển thị DataFrame
st.subheader('10 Random Rows with Predictions')
st.write(display_df)