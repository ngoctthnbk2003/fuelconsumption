Các bước thực hiện 
1. Tạo thư mục con pictures cùng với các file trong cùng thư mục
2. Chỉnh sửa đường dẫn file dữ liệu
3. Chạy file HeHoTroRaQuyetDinhNhom30.ipynb
4. Chạy file app.py (đang sử dụng mô hình random forest để chạy web app)
5. Tiếp tục chạy câu lệnh "streamlit run app.py" trong terminal, sau đó tự động hiển thị trang web 
6. Kiểm tra dữ liệu mong muốn trên web